FROM python

WORKDIR /app

COPY . .

RUN pip install fastapi
RUN pip install uvicorn

CMD ["uvicorn", "main:app", "--reload", "--workers", "1", "--host", "0.0.0.0", "--port", "8000"]
