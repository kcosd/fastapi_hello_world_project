from fastapi import FastAPI


app = FastAPI()


# uvicorn main:app --reload

@app.get('/test')
def home():
    return {"Hello": "World"}
